﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    public class Consts
    {
        public const int FF = 255;
        public const int C0 = 192;
        public const int OFFSET = 3;
        public const int BITS_PER_COMPONENT = 8;
    }
}
