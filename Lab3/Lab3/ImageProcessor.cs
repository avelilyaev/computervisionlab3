﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3
{
    public class ImageProcessor
    {
        public Int32 Width { get { return _width; } }
        public Int32 Height { get { return _height; } }
        public Int32 ColorDepth { get { return _colorDepth; } }

        public ImageProcessor(PictureBox pBox)
        {
            PBox = pBox;
            PBox.Image = Image = new Bitmap(PBox.Width, PBox.Height);
        }

        public void SetImage(FileStream stream)
        {
            ReadParameters(stream);

            PBox.Image = Image = (Bitmap)Bitmap.FromFile(stream.Name);
            PBox.Invalidate();
        }

        public void LinearStretching()
        {
            PBox.Invalidate();
        }

        private void ReadParameters(Stream stream)
        {
            for (int i = 0; i < stream.Length; i++)
            {
                //marker 0xFFC0
                if (stream.ReadByte() == Consts.FF && stream.ReadByte() == Consts.C0)
                {
                    stream.Position += Consts.OFFSET;
                    _height = ReadField(stream);
                    _width = ReadField(stream);
                    _colorDepth = stream.ReadByte() * Consts.BITS_PER_COMPONENT;
                }
            }
        }

        private int ReadField(Stream stream)
        {
            byte[] buf = new byte[2];
            stream.Read(buf, 0, 2);
            return (buf[0] << 8) | buf[1];
        }

        private Bitmap Image { get; set; }
        private PictureBox PBox { get; set; }
        private Int32 _width { get; set; }
        private Int32 _height { get; set; }
        private Int32 _colorDepth { get; set; }
    }
}
