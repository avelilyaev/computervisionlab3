﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3
{
    public partial class Form1 : Form
    {
        private ImageProcessor imageProcessor;

        public Form1()
        {
            InitializeComponent();
            imageProcessor = new ImageProcessor(pictureBox1);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Text = "Выберите полутоновое изображение";
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LoadImageButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            FileStream stream;

            if (fd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((stream = (FileStream)fd.OpenFile()) != null)
                    {
                        Text = stream.Name;

                        string extension = Text.Split(new char[] { '.' })[1];
                        if (extension != "jpeg" && extension != "jpg")
                        {
                            throw new Exception("Загруженный файл должен соответствовать формату \"JPEG\"");
                        }

                        imageProcessor.SetImage(stream);

                        label1.Text = string.Format("Разрешение: {0}x{1} пикселей, глубина цвета: {2} бит", imageProcessor.Width, imageProcessor.Height, imageProcessor.ColorDepth);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void LinearStretchingButton_Click(object sender, EventArgs e)
        {
            imageProcessor.LinearStretching();
        }
    }
}
